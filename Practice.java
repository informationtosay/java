public class Practice// class is imaginary memory,combination of memory allocation by data storage and value stroge in ref variable or ref name. class name mention meaning full name, 1st letter start to capital,
{
    static String cityname="Thuraipakkam";//static variable
           String book;// non static variable
	       int price;//non static variable
public Practice(String book,int price)//constructor name same as class name
{
      this.book=book;//this is a reference varible, refer to current object, eliminate confusion b/w class attributes and parameters
	  this.price=price;//(;)semicolon -That is, each individual statement must be ended with a semicolon. It indicates the end of one logical entity.
	           
	System.out.println(book);// book parameter o/p, (S.o.p)-prints a message to the standard output 
}	
public Practice(String book)//An argument is a value passed to a function when the function is called. Whenever any function is called during the execution of the program there are some values passed with the function. These values are called arguments.
{
      this.book=book;     
	    System.out.println(book);

}	
public static void main(String[] args)// Void -keyword acknowledges the compiler that main() method does not return any value.The main() is the starting point for JVM to start execution of a Java program. Without the main() method, JVM will not execute the program. 
	
{
	int target=100000;//local variable
	
  Practice res1=new Practice("Ramayanam",480);  //object means memory allocation by data storage and value stroge in ref variable or ref name, object representation of class
  Practice res2=new Practice("Mahabharatham",550);
  Practice res3=new Practice("Thirukural",1000);
  Practice res4=new Practice("Sellatha Panam");	

    System.out.println(res1.book);//non static variable -calling to object  
      System.out.println(cityname);//static var o/p
                System.out.println(target);//local var o/p

}	
public int age()
{
	return 10;



}	

}



















































